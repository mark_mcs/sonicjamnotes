Stored in SETDATA.MUS. Loaded into 0x202000 by the menu system and then copied into place at 0x60c8000 by the Sonic World startup routines.

See [[Object Spawning#Spawner Meta-Object]] for details on how this is parsed and loaded at runtime.

The layout file format allows for 2048 objects per level but the engine maintains an internal list of active objects that's only 512 entries long.

```c++
struct object_entry {
    byte flags;
    byte type;

    /* position is stored as the integer part of a FIXED type. 
     * i.e. the upper 16 bits.
     */
    ushort x_pos;
    ushort y_pos;
    ushort z_pos;

    /* The upper byte of an ANGLE type. */
    byte x_rotation;
    byte y_rotation;
    byte z_rotation;

    byte ???;  // copied to field 0x08
    byte ???;  // copied to field 0x09
    byte ???;  // copied to field 0x0a

    /* byte offset to the next entry in the list */
    short next_offset;
}
```

#### Object Flags Format
The first field in the object_entry struct is a bitfield with some flags that are set in the file and some that are set by the engine at runtime.

| Bit | Description                                                                                                                                        |
| --- | -------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0   | "active" flag. Set by the engine when the player is within the object's activation radius and the engine has loaded the object into the playfield. |
| 1   | "collected" or "destroyed". The object won't be respawned if it comes back into range later.                                                       |
| 7   | Signals the end of the list when set.                                                                                                              |

When MSB of object type is set it signals the end of the list.