The map is divided up into a grid of 7x8 cells of 1,024 units in size.
## Terrain System Memory Layout
```c++
/* allocated into one of the object slots */
struct terrain_system {
    /* holds the index of the grid cell that is currently being rendered.
     * this gets updated by the function at 0x060519f4.
     */
    int current_grid_cell;

    int ???;

    // all remaining memory in the object slot is unused.
}
```

## Visible Region Calculation

Structures at 06051ce8
```c++
struct visible_chunk {
    /* The index of a cell in the 7x8 grid that this region represents. */
    char grid_cell_hash;

    char
};
```