
0x00 - header size
0x01 - checksum

header size + 2 (0x24) - bit fields

5 bits = offset into array 0x60efe00
repeat:
  3 bits = if != 7 value to store in array
		   if 7 shift bitbuf and count sequential 1 bits. this is the
			 number of bits to skip over in bitbuf. number of skipped
			 bits gets written to the array.
ends with:
  2 bits = number of padding bytes to write

