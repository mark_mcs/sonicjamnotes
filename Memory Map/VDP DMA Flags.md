
| Bit | Description |
|----|----|
| 2 | Triggers DMA0 in indirect mode from a preconfigured buffer of commands and updates VDP2 VRAM. |
| 3 | When cleared causes the ObjColourcalc constructor to run |

\
