### Sonic World Overlay
##### Low Work RAM
| Start    | End      | Size   | Description                                         |
| -------- | -------- | ------ | --------------------------------------------------- |
| 0x200000 | 0x208000 | 0x8000 | Level object layout buffer. Loaded from SETDATA.MUS |
| 0x2C0000 |          |        | ??? Loaded from SCRDATA.MUS VDP2 scroll data?       |
|          |          |        |                                                     |

##### Hi Work RAM
The Sonic World overlay's work RAM starts at 0x60C8000 and extends up to 0x6100000 (approx 229k).

| Start     | End       | Size    | Use                                                                                            |
| --------- | --------- | ------- | ---------------------------------------------------------------------------------------------- |
| 0x6001000 |           |         | Slave stack                                                                                    |
| 0x6040000 |           |         | Overlay load address                                                                           |
|           |           |         |                                                                                                |
| 0x607F968 | 0x60B06D3 | 0x30D6C | Model data                                                                                     |
|           |           |         |                                                                                                |
| 0x60C8000 | 0x60D0000 | 0x08000 | Holds the level object layout data. Copied from a pre-populated buffer at 0x202000.            |
| 0x60D0000 | 0x60D8FFF | 0x09000 | Game object fixed-size slots (384 of them).                                                    |
| 0x60D9000 | 0x60D9FFF | 0x01000 | Colour RAM (VDP2) DMA buffer                                                                   |
| 0x60DA000 | ???       | 0x0ED8+ | [[Sprite Texturing#Texture ID to Address Mappings]]                                            |
|           |           |         |                                                                                                |
| 0x60DC800 |           |         | Primary gouraud table DMA transfer buffer.                                                     |
| 0x60DCC00 |           |         | Secondary gouraud table DMA transfer buffer.                                                   |
| 0x60DD000 | 0x60DEFFF | 0x02000 | Hash table of depth-sorted polygons for the master CPU. See [[Depth Sorting]]                  |
| 0x60DF000 | 0x60E0FFF | 0x02000 | Hash table of depth-sorted polgyons for the slave CPU. See [[Depth Sorting]]                   |
| 0x60E1000 | 0x60E2FFF | 0x02000 | Buffer to store transformed vertices while processing a model. Master CPU version.             |
| 0x60E3000 | 0x60E4FFF | 0x02000 | Buffer to store transformed vertices while processing a model. Slave CPU version.              |
| 0x60E5000 | 0x60E5FFF | 0x01000 | Array of polygon_visibility_info structures used while processing a model. Master CPU version. |
| 0x60E6000 | 0x60E6FFF | 0x01000 | Array of polygon_visibility_info structures used while processing a model. Slave CPU version.  |
| 0x60E7000 |           |         | Array of command_table structures to be DMAed to VRAM.                                         |
|           |           |         |                                                                                                |
| 0x60F4690 |           |         |                                                                                                |
|           |           |         |                                                                                                |
| 0x60F4C2C | 0x60F4E2B | 0x00200 | Temporary buffer used for lighting calculations on the master CPU. Array of vertex colours.    |
| 0x60F4E2C | 0x60F502B | 0x00200 | Temporary buffer used for lighting calculations on the slave CPU. Array of vertex colours.     |
|           |           |         |                                                                                                |
| 0x60F542C | 0x60FF8FC | 0x0A4D0 | Memory arena for the game object allocator. This seems to overlay some other variables.        |
|           |           |         |                                                                                                |
| 0x60FFC00 | 0x60FFF8F | 0x00390 | [[System Variable Area]]                                                                       |
| 0x60FFF90 | 0x60FFFCF | 0x00040 | Head pointers for each of the game object linked lists.                                        |
| 0x60FFFD0 | 0x60FFFD3 | 0x00004 | Head pointer for the game object free list.                                                    |
| 0x60FFFD4 | 0x60FFFD7 | 0x00004 | Count of active objects across all object lists.                                               |
|           |           |         |                                                                                                |


#### VDP1 VRAM
| Start     | End       | Size    | Use                                               |
| --------- | --------- | ------- | ------------------------------------------------- |
| 0x5c00000 | ???       | ???     | Command tables.                                   |
|           |           |         |                                                   |
| 0x5c0c000 | 0x5c0c100 | 0x100   | Player model gouraud table. Cleared by 0x06044064 |
|           |           |         |                                                   |
| 0x5c0c800 |           | 0x100?? | Tails model gouraud table.                        |
|           |           |         |                                                   |
| 0x5c0d040 | 0x5c78068 | 0x6B028 | Textures and CLUTs from SPRDATA.MUS               |


#### VDP2 VRAM
| Start       | End       | Size   | Use               |
| ----------- | --------- | ------ | ----------------- |
| 0x5e42000   | 0x5e44000 | 0x2000 |                   |
| > 0x5e42480 | 0x5E42730 | 0x02B0 |                   |
| 0x5e44000   | 0x5e48000 | 0x4000 |                   |
|             |           |        |                   |
| 0x5e5f800   | 0x5e5fbc0 | 0x3C0  | line window table |
| 0x5e60000   | ???       | ???    | coefficient table |

