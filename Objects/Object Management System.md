TODO: object in slot 6 runs as part of vblank. what is this?

## Object Lists
Game objects are organised in doubly-linked lists, with each list holding a different category of objects. For example, there's a list that contains the player object(s); a list for the stage obstacles; etc.

There is a fixed size array of ObjectList structures at 0x60FFF90 -> 0x60FFFD0, each of which hold the head & tail pointers for a list:

```c++
  struct ObjectList {
    Object* head;
    Object* tail;
  };
```

The objects themselves are fixed-size, 96 byte, blocks of memory between 0x60D0000 and 0x60D9000 (384 blocks). The first 16 bytes contain the link pointers & other data common to all objects. The remaining 80 bytes are free for the object to use as necessary.

```c++
struct ObjectNode {
    ObjectNode* next;
    ObjectNode* prev;

    /* contains the low 16-bits of the address of this object within the
     * [[Object Layouts (SETDATA.MUS)]]. Not sure what this is for
     * hardcoded objects (e.g. camera, player, tails, etc).
     */
    ushort lvl_layout_offset;

    /* holds the activation radius of the object. the scene's target
     * object must be within this to cause the object to load.
     */
    ushort activation_radius;

    /* pointer to the handler function that will be run each update cycle.
     */
    void (*handler)(void *private_data);

    /* remaining space is for object-specific data */
    char private_data[80];
};
```

The last ObjectNode in the list has a negative number as the ```next``` pointer. The engine uses this to work out where the owning ```ObjectList``` is so that it can update the list's tail pointer.

Handler functions are the logic that the object executes on each update cycle. Following the SH2 standard calling convention, a pointer to the object's private data struct is passed to the function in the r4 register, but there's also a copy in r14. Many handler functions seem to prefer the copy in r14.

## List Types
- List 0
    - Camera
    - Spawner
- List 1
    - Sonic
    - Big Ring
    - Tails
    - Tails (far - billboard object)
- List 2
    - anything created by spawner (level objects)
- List 3
    - Terrain system/renderer
    - Player model renderer
    - Ring sparkle
- List 4
- List 5
    - Skybox/floor plane system
    - Fader
## Object Allocation
The engine maintains a free-list of ObjectNodes that it can pull from whenever it needs to allocate an object. This is just another doubly-linked list, located at 0x60FFFD0, that chains together ```ObjectNode```s using their ```next```/```prev pointers```.