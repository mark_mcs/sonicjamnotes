Objects can be made to move in a fixed path around the level. These paths are pre-recorded sequences of 3D positions and rotations that are copied to the object's position & rotation. The objects keeps track of their position on the path using a state struct at offset 0x44 in their private data.

```c++
struct path_elem {
    // rotation components are the upper 8 bits of an ANGLE type
    int8_t x_rotation;
    int8_t y_rotation;
    int8_t z_rotation;

    char _padding

    // position components are the upper 16 bits of a FIXED type
    int16_t x;
    int16_t y;
    int16_t z;

    int16_t tween_step;
};
```

```c++
struct path {
    path_elem *elements;
    int32_t count;
}
```