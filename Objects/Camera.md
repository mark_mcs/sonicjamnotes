Usually allocated first so ends up at 0x60D0000. After 16 bytes of housekeeping data, camera data structures start at 0x60D0010.

| Offset | Type   | Description                                                                                                        |
| ------ | ------ | ------------------------------------------------------------------------------------------------------------------ |
| 0x00   | byte   | flags                                                                                                              |
| 0x01   | byte   | flags?                                                                                                             |
|        |        |                                                                                                                    |
|        |        |                                                                                                                    |
| 0x04   | ANGLE  | y-rotation delta. gets added to offset 0x1A                                                                        |
|        |        |                                                                                                                    |
| 0x08   | long   | distance from target object.                                                                                       |
| 0x10   | long   | y-offset relative to player object position. i.e. position above/below player.                                     |
|        |        |                                                                                                                    |
| 0x18   | ANGLE  | x-rotation                                                                                                         |
| 0x1A   | ANGLE  | y-rotation                                                                                                         |
| 0x1C   | ANGLE? | z-rotation?                                                                                                        |
|        |        |                                                                                                                    |
| 0x20   | Vec3D  | Last seen target position. If the target's current position matches this then the camera doesn't need to be moved. |
|        |        |                                                                                                                    |
| 0x30   | Vec3D  | Position                                                                                                           |
| 0x3C   | ???    |                                                                                                                    |
|        |        |                                                                                                                    |
