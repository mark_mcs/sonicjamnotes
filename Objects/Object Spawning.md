The engine makes use of a number of 'special' objects that don't have a physical presence (per se) in the level. These can be used, for example, to trigger events when a player reaches a certain point, or even just to execute some piece of code on each update cycle.

### Spawner Meta-Object

The Spawner object is used to execute the object spawning function on each update cycle. The function traverses the level's object list, compares each object's position with the scene's current target object (usually the player but doesn't have to be), and if the target is within the spawn radius of the object it gets loaded into the level.

##### Spawner Object Structure

```c++
struct obj_spawner {
    // pointer to an array of structures that the spawn function will
    // use to load the objects into the level. it contains things like
    // the object's name, pointer to the handler function, etc.
    void *object_definitions;
};
```

```c++
// 32 byte struct
struct object_definition {
    /* pointer to the handler function for this object type */
    void (*handler)(private_data *);

    / * gets copied into object field 0x1e  - gouraud data? */
    short ???;

    /* this object will activate when the target is within this radius */
    short activation_radius;

    char name[8];

    /* remaining 16 bytes are parameters that are made available to the handler
     * function. a pointer to this data gets stored in the first field of the
     * object's private data struct.
     */
    void* params[4];
};
```

\
