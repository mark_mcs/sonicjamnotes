## Handlers
The main handler function puts pointers to some frequently used data structures into registers. Following subroutines expect those register allocations.

| Reg | Data Structure                          |
| --- | --------------------------------------- |
| r1  | ObjPlayer*                              |
| r12 | [[Player Object#Model Animation State]] |
| r13 | Struct at 0x10                          |

## Data Structures
Usually allocated in slot 1 (after camera in slot 0) - 0x60D0060.

Offsets relative to private data area at 0x60D0070

| Offs | Type      | Description                                                                                  |
| ---- | --------- | -------------------------------------------------------------------------------------------- |
|      |           |                                                                                              |
| 0x08 | ptr       | points to [[Player Object#Model Animation State]] struct                                     |
|      |           |                                                                                              |
| 0x0A | byte      | seems to be a collision flag?                                                                |
|      |           |                                                                                              |
| 0x0D | byte      | some sort of flags. 0x4 disables lighting?                                                   |
| 0x0E | byte      | used to select from a list of model pointers (0x604cab2)                                     |
|      |           |                                                                                              |
| 0x10 | pointer   | pointer to struct                                                                            |
| 0x14 | pointer   | pointer to the submodel_orientation struct.                                                  |
| 0x18 | ANGLE     | Current x-rotation                                                                           |
| 0x1A | ANGLE     | Current y-rotation                                                                           |
| 0x1C | ANGLE     | Current z-rotation                                                                           |
| 0x1E | ushort    | Gouraud table base address. Faces in the model will offset from this to get to their tables. |
| 0x20 | Vec3D     | If this is set to a positive value it skips setting a y-acceleration value at 0x604e786      |
| 0x2C | FIXED     | Model scale factor.                                                                          |
| 0x30 | Vec3D     | Position                                                                                     |
|      |           |                                                                                              |
| 0x40 | pointer   | Gouraud shading data. gets copied into SVA 0x230. buffer of 0x800 bytes.                     |
| 0x44 | path_data | structure holding state data for the path that the object should follow.                     |

#### Flags 0x0D
| Bit | Description                                     |
| --- | ----------------------------------------------- |
| 0   |                                                 |
| 1   | 0 = flat shaded polys, 1 = gouraud shaded polys |
|     |                                                 |


### Path State
```c++
struct path_state{
    char flags;
    char padding[3];
    void *path_data;
    int32_t tween_value;
}
```

#### Flags
| Bit | Description                                             |
|-----|---------------------------------------------------------|
|  0  | Restart mode. 0 = Loop back to start. 1 = Stop at end.  |


### Model Animation State
Gets allocated at 0x060F543C.
| Offs | Type  | Description                                                                                              |
| ---- | ----- | -------------------------------------------------------------------------------------------------------- |
| 0x00 | byte  | Player object state.                                                                                     |
| 0x01 | byte  | Flags bitfield                                                                                                         |
|      |       |                                                                                                          |
| 0x07 | byte  | flags                                                                                                    |
| 0x08 | byte  | Next model ID. The model to use for the next frame. Gets copied into 0x09 when the model is loaded.      |
| 0x09 | byte  | Current model ID. The model that is being used for the current frame.                                    |
| 0x0A | byte  | Some sort of "has model" flag? If > 0 model data is loaded (0604dc08)                                    |
| 0x0B | byte  | Animation lerp step counter. When this reaches 0 the animation frame is advanced.                        |
| 0x0C | short |                                                                                                          |
|      |       |                                                                                                          |
| 0x14 | short | The next animation number. This will be copied into 0x16 the next time the animation logic is processed. |
| 0x16 | short | The current animation number.                                                                            |
| 0x18 | short | The index of the frame within the animation that is currently being used.                                |
|      |       |                                                                                                          |
| 0x28 | int   | some sort of counter. gets incremented when updating the model matrix                                    |
| 0x2C | int   | holds a counter that's used by the tails object to make the model bob up and down.                       |
|      |       |                                                                                                          |
|      |       |                                                                                                          |


#### Flags at 0x01
| Bit  | Description                        |
| ---- | ---------------------------------- |
|      |                                    |
| 0x20 | Ignores controller input when set. |
|      |                                    |
|      |                                    |
|      |                                    |

### Struct pointed to by 0x10
| Offs | Type  | Description                                                                                                                               |
| ---- | ----- | ----------------------------------------------------------------------------------------------------------------------------------------- |
|      |       |                                                                                                                                           |
| 0x0A | ANGLE | The angle on the y-axis that the object is rotating towards The object's y-rotation will be updated in steps until it reaches this value. |
|      |       |                                                                                                                                           |
| 0x10 | ANGLE | Used as an adjustment to the object's main z-rotation.                                                                                    |
|      |       |                                                                                                                                           |
| 0x18 | int   |                                                                                                                                           |
| 0x1C | int   |                                                                                                                                           |
| 0x20 | int   | related to 0x20 in object private data                                                                                                    |
| 0x24 | Vec3D | Acceleration vector. Added to object's velocity vector.                                                                                   |
|      |       |                                                                                                                                           |
|      |       |                                                                                                                                           |

### Submodule Orientation Data
| Offs | Type     | Description                                                                   |
| ---- | -------- | ----------------------------------------------------------------------------- |
| 0x00 | Vec3D    | Added to an object's position. ???Seems to be ignored & hardcoded instead.??? |
| 0x0C | ANGLE[3] | Rotation data for model part 0, in (z, y, x) order.                                               |
| 0x14 | ANGLE[3] | Rotation data for model part 1.                                               |
| ...  |          |                                                                               |
|      | ANGLE[3] | Rotation data for model part n.                                               |

### Gouraud Shading Data (Struct at 0x40)
This data structure holds the gouraud tables for each of the polygons in the object's model.
```C++
struct gouraud_table {
    ushort corners[4];
};
```
This is a large buffer that holds a mix of lighting lookup tables (possibly for material specular lighting) and buffers for gouraud tables for each face.

| Offs | Type         | Description                                                                                          |
| ---- | ------------ | ---------------------------------------------------------------------------------------------------- |
| 0x00 | ushort[0x80] | Gouraud table entries???                                                                             |
| 0x80 | ushort[0x80] | A lookup table of lighting values. Dot product is quantized and used as an index. Material specular? |
|      |              |                                                                                                      |


## Sonic Animation IDs
| ID  | Description        |
| --- | ------------------ |
|     |                    |
| 6   | Jumping ball model |
|     |                    |