### Parameters
| Param | Description                                     |
| ----- | ----------------------------------------------- |
| 0x00  | Sprite info table. Gets copied into field 0x2C. |
| 0x04  | Gets copied into field 0x20.                    |
| 0x08  | Unused                                          |
| 0x0C  | Unused                                          |

### Object Memory Layout
| Field | Type              | Description |
| ----- | ----------------- | ----------- |
| 0x0e  | ushort            |             |
|       |                   |             |
| 0x12  | ushort            |             |
|       |                   |             |
| 0x20  | uint              |             |
|       |                   |             |
| 0x2C  | sprite_info table |             |
|       |                   |             |
