### Private Data Struct
| Offset | Type            | Description                         |
| ------ | --------------- | ----------------------------------- |
| 0x00   | short           | Rotation speed.                     |
| 0x02   | ANGLE           | Angle of the spinning top part      |
|        |                 |                                     |
| 0x20   | model*          | Model for the bottom billboard part |
| 0x24   | model*          | Model for the rotating top part.    |
| 0x28   | collision_data* | Collision box data.                 |