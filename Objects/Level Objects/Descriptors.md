Level objects are loaded into the playfield dynamically, based on the camera/scene target's position on the map.  Regardless of type, they have a shared private data format:

```c++

struct lvl_obj_private_data {
    /* points to the extended attributes (16 bytes) for this instance that are
     * stored in the object definition table. */
    char *attributes_ptr;

    // 0x0d - flags that control various aspects of the rendering process.
    //  bit 0:
    //  bit 1: shading mode. 0 = flat, 1 = gouraud
    //  bit 2: set to enable gouraud shading/lighting
    byte render_flags;
    // 0x0e - current position within the animation sequence.
    byte amin_sequence_idx;
    // holds a display time counter that gets decremented each frame.
    // when the counter hits 0 the next animation frame is displayed.
    byte frame_display_count;

    // 0x12
    // for sprite-based 2D objects, this is the index of the animation frame
    // to display. It's used to index into the sprite info list using the
    // base pointer at 0x2C.
    ushort anim_frame_number;
    // 0x14
    // an 8.8 fixed point value that acts as a multiplier for the sprite size.
    ushort sprite_scale_factor;

    ushort field_0x1e;

    // 0x28 - gets set for the ring object. common???
    FIXED distance_from_ground;
    // 0x2C
    // points to an array of shorts that are offsets to texture info structs.
    // see [[Sprite Texturing]]
    short *sprite_info_table;
    // 0x30
    Vec3D position;
    // 0x3C - pointer to the animation sequence data. The field at offset
    //        0x0e is used to index into this.
    char * anim_sequence_ptr;

    // 0x40 - gouraud table data for the object's 3D model. This array is
    // split into 2 halfs - each with 0x80 bytes - and is used as a double-
    // buffer. The array is ordered by polygon, so the gouraud data for 
    // polygon 0 is at gouraud_data[0], the data for polygon 1 is at
    // gouraud_data[1], and so on.
    gouraud_table *gouraud_data;
    
    // 0x44 - path state info. keeps track of where the object is along
    //        a fixed path (only used for Tails object)
    struct path_state {
        // 0x00
        char flags;
        // <padding>
        // 0x04 - pointer to the path that this object is using.
        path *data;
        // 0x08 - current position along the path.
        uint16_t position;
        uint16_t ???
    } path_state;
};

```

The attributes pointer points to an array of 4 other pointers.

```c++
struct collision_data {
    FIXED xz_radius;
    FIXED y_radius;  // squared
    FIXED xz_projection;
    FIXED y_projection;
};

struct model_data {
    vec3 *vertices;
    uint vertex_count;
    
};

struct object_attributes {
    model_data *model;
    void *b;
    void *c;
    collision_data *collision;
};
```