### State Values
Values for the animation state (obj descriptor offset 0x08) `state` field.
| Value | Description       |
| ----- | ----------------- |
| 0x00  | Idle              |
| 0x01  | Walking           |
| 0x02  | Running           |
| 0x04  |                   |
| 0x05  | Jumping           |
|       |                   |
| 0x0B  | Flying with Tails |