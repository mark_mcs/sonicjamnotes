### Gouraud Tables
Each game object allocates a [[Player Object#Gouraud Shading Data Struct at 0x40|buffer]] in RAM to hold gouraud shading data for its model. This gets filled by the transform & lighting task (0x06044b92) and then DMAed to VRAM during vblank processing.

The RAM buffers are linked to VRAM addresses via ```gouraud_tbl_shadow_link``` structures that are kept in 2 stacks. These are scanned in the vblank handler to get the write address for each DMA transfer.

```c++
struct gouraud_tbl_shadow_link {
    uint count;
    gouraud_entry *vram_base;
    gouraud_entry *ram_base;
};
```

There are 2 stacks (why???) of shadow links: 0x060dc800 and 0x060dcc00 which are indexed by SVA variables 0x120 and 0x122. The active stack is switched based on the value of SVA variable 0x009. See function 0x06040970.