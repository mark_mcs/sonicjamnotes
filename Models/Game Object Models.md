```c++

/* This is the SGL "POLYGON" type but without the "norm" member.
 * Normals are stored in a separate table.
 */
struct face {
    short indices[4];
};

struct face_attrs {
    /* The only flag that's in use is the "plane" attribute that
     * decides whether a polygon is single sided (subject to backface
     * culling) or double sided (not culled).
     */
    byte flags;
    
    /* The depth sorting method for this face. One of SORT_BFR,
     * SORT_MIN, SORT_MAX, SORT_CEN.
     */
    byte sort_method;
    
    /* will be used to index into a table of texture info at 0x60DA000.
     * IDs start from 1 as 0 means an untextured part.
     */
    short texture_id;
    
    /* copied into CMDPMOD field of the command table */
    short draw_mode;
    
    /* copied into CMDCOLR field of the command table, possibly
     * with modifications due to depth shading.
     */
    short colour;
    
    /* Added to the game object's gouraud base address to get to the
     * shading data for this face.
     */
    ushort gouraud_table_offset;
    
    /* copied to the CMDCTRL field of the command table */
    short cmdctrl;
    
    short field_0x0c;
    
    short field_0x0e;
};

/* Corresponds to the SGL "XPDATA" struct. */
struct model_part {
    vec3d* vertex_list;
    uint vertex_count;
    face* face_list;
    uint face_count;
    face_attrs* attrs;
    vec3d* normals;
};

struct model {
    model_part* parts;
}
```

In the face structure, vertex indices [0] and [2] can be negative.

Negative vertex[0] triggers some special processing when the projected vertices are being copied into the VDP1 command table. 

Negative vertex[2] triggers some special processing when the polygon is being clipped.

Actual vertex indices can be recovered from negative values by doing ``` index = -index + 8```
