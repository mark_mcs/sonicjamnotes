## Model Rendering
LAB_SONICWORLD__0604ccbe

### Slave Transform Task
Task parameter buffer structure
```c++
struct {
    /* Pointer to the player object */
    object* obj;
    
    /* pointer to the model part to be rendered */
    model_part* part;

    /* looks like the gouraud shading tables for the model being rendered */
    void* obj_field0x40; // copied from GBR + 0x230

    /* seems to be used as a pointer to face gouraud/colour data? */
    int r6;     // set to 0
    
    /* Polygon winding comparison type.
     * the lower byte is an opcode for a conditional branch
     * with displacement. this used to overwrite an instruction
     * and dynamically change the control flow.
     * 0x89 == bt (2*disp)
     * 0x8B == bf (2*disp)
     *
     * bt = CW winding, bf = CCW winding
     */
    int branch_instr;     // set to 0xFFFF_FF8B
    
    /* copied from the top of the matrix stack when the task
     * is dispatched
     */
    mat3x4 model_view_mtx;
}
```

