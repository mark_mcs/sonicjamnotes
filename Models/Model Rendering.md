Vertex projection phase uses a work buffer at 0x60E1000 to store the transformed coordinates and depth values.
```c++
struct screen_coords {
    // holds the value of the transformed vertex's z-coordinate with the
    // lower 4 bits masked out.
    // If the vertex was clipped against the edge of the screen the lower
    // 4 bits will hold the flags that show which edges it was clipped against
    // and then the whole value will be negated.
    int32_t depth;
    // 2D screen-space coordinates
    uint16_t x;
    uint16_t y;
};
```

After vertex projection the polygons are assembled based on the model's face defintions and visibility calculations are performed. A buffer at 0x60E5000 is used to store these structures. The list of structures is terminated with -1 where the next `sum_of_depth` field would be.
```c++
struct polygon_visibility_info {
    // holds the value that represents the z-order position of the polygon.
    // this is calculated based on the polygon's 'sort' attribute, which is
    // either SORT_MIN, SORT_MAX, SORT_CEN, or SORT_BFR.
    int32_t depth_value;
    // a bitfield that shows which verts were clipped.
    // 0x8 = vertex 0, 0x
    // if this field is non-zero then the resulting VDP1 command table
    // will have the CMDPMOD.PCLP bit set
    uint8_t clip_flags;
    // A counter that starts at -2 and is increased by 1 for each
    // vertex that's outside the visible screen area
    int8_t vertex_clip_count;
    // some sort of offset value. gets incremented by 8 for each
    // polygon that gets processed.
    // it's an offset to a vertex index in a face
    uint16_t vertex_offset;
};
```