### Animation Header
```c++
struct anim_header {
    short* frame_table;
    int ???;
    short lerp_steps;
    short end_type;
    short end_type_arg;
    short ???;
};
```

| Field        | Description                                                                                                                                                                                                               |
| ------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| frame_table  | An array of signed 16 bit offsets into the frame data stream. offsets are the start of data for each frame.                                                                                                               |
| lerp_steps   | The number of interpolation steps leading up to this keyframe. Gets copied into [[Player Object#Model Animation State]] lerp step counter. This field is a short but only the lower byte is copied. Must be a power of 2. |
| end_type     | decides what happens when the last frame in the anim is reached.                                                                                                                                                          |
| end_type_arg | parameter that can be interpreted based on the end_type. only used by end_type=4 where it holds the ID of the next animation.                                                                                             |

#### End Type
- **Type 0**: Repeat last frame.
- **Type 2**: Restart at frame 0.
- **Type 4**: Change to animation. Sets the object's animation to `end_type_arg` and restarts at frame index 0.
- **All other types**: Continue on to the next frame in the stream.


### Frame Data Format
First ushort is a set of flags. If the flag is clear then that particular element is present in the data stream. If set the element is not present and nothing needs to be read.

| Bit | Description                     |
| --- | ------------------------------- |
| 0   | Remaining X-rotation components |
| 1   | Remaining Y-rotation components |
| 2   | Remaining Z-rotation components |
| 3   | Z-translation component         |
| 4   | Y-translation component         |
| 5   | X-translation component         |
| 6   | First X-rotation component      |
| 7   | First Y-rotation component      |
| 8   | First Z-rotation component      |

The data stream that follows is based on the frame format. Translation components are first in the stream, with 4 bytes (FIXED) for each entry. 16 sets of rotation components follow with 2 bytes (ANGLE) per entry. The first set of rotation components follows the format defined in bits 6, 7 & 8; remaining components follow the format in bits 0, 1 & 2.