## Texture ID to Address Mappings
Sprites reference textures using an integer ID which need to be translated into addresses that can be put into a VDP1 command table's CMDSRCA field. This information is kept in an array of structures based at 0x60DA000. The ID is used to index into this array and the mapping data is used to populate the command table (see function at 0x060462e2).

```c++
struct texture_id_mapping {
    ushort cmdsrca;
    ushort cmdsize;
    ushort cmdcolr;
    ushort ???;
};
```

## Colour Conversion
The engine supports a colour conversion function that can replace or duplicate a texture mapping. The function at 0x06044630 has the follwing signature:

```c++ 
struct conversion {
    ushort from_cmdcolr;
    ushort to_cmdcolr;
};

void sw_convert_texture_colour(ushort *texture_id_list, texture_id_mapping *dest_texture, conversion *conversion_table);
```

```texture_id_list``` is an array of indices into the mapping data, terminated with 0xFFFF.

```dest_texture``` is a pointer to an array of mapping data at which the converted records are to be written.

```conversion_table``` is an array of ushort[2], where [0] is the key and [1] is the conversion value. It is searched using the source cmdcolr as the key to find the conversion value. A key of 0xFFFF triggers special handling for RGB colours (TODO: document).

## Textured Sprite Objects
Each animation frame for a sprite-based 2D object can be made up of multiple VDP1 "draw normal/scaled/distorted sprite" commands. In order to set up the command tables at render time the object's data structure contains a reference to a list of structs that hold texture ID, size, relative position, etc. info.

The object field at 0x2c points to a base address. At that address are an array of shorts that offset from that base to point to an array of structs that hold the texture info for each sprite. It looks like this:

```
// offsets to sprite info arrays
0x1000: 0x0010   // offset to sprite info list for first anim frame
0x1002: 0x0054   // list for second frame
0x1004: 0x00A0   // list for third frame
  ...
0x1010: sprite_info {...}
0x1030: sprite_info {...}
0x1050: 0xFFFF
  ...
0x1054: sprite_info {...}
0x1074: 0xFFFF   // last entry in the list terminated with (short)-1
  ...
etc
```

Where each ```sprite_info``` is:

```c++
struct sprite_info {
    short texture_id;

    // the position of this sprite relative to the object's position
    short x_offset;
    short y_offset;

    short cmdpmod;

    char ???

    // VDP1 command type
    char cmdctrl;

   ...
};
```

## Animation Sequences
The frames described by the sprite info tables (above) are linked together into an animation sequence by way of a table of frame indices and display counts. The index is used to select one of the frames to display and the counter represents how many game ticks that frame should be shown. 

The end of the animation sequence is indicated by a negative value (the engine uses 0xFF/-1 but any negative value works), which causes the sequence to restart from the beginning.

Example:
```
.db 0x00, 0x02  // display frame 0 for 2 game ticks
.db 0x01, 0x02  // display frame 1 for 2 ticks
.db 0x02, 0x02  // display frame 2 for 2 ticks
.db 0xFF        // end of list - loop back to sequence entry 0
```