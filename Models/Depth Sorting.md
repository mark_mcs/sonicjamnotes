Polygons need to be sorted so that those that are furthest from the camera are drawn first and then nearer polygons are successively layered on top (the painter's algorithm). This is achieved by setting the CMDLINK field of each command table to point to the next closest polygon. Once VDP1 is done with a command it will follow the link to the next one, thus drawing order can be controlled.

### Depth Sort Table
As models are submitted for rendering, the rendering code needs to calculate a depth value for each polygon in the model and then work out where they fit in relation to other polygon commands that have already been submitted. To do this, each polygon is inserted into a sorted hash table, using the depth value for the polygon as the key.

The hash table is structured as an array of linked lists, where the upper 16-bits of the polygon's depth (the integer part) are used to index into the array. The lower 16-bits (the fractional part) are used to find the correct position to insert the polygon into the linked list.

The structure looks like this:

```c++
/* The elements of the hash table array (the buckets) hold the pointers
 * to the linked list.
 */
struct depth_bucket_node {
    /* The head of the linked list. should have the highest depth value
     * (farthest from the camera) of all of the tables in this chain.
     */
    command_table* head;
    
    /* The tail of the linked list. This has the lowest depth value
     * (closest to the camera).
     */
    command_table* tail;
};

struct depth_hash_table {
    /* Depths between 0-1024 are valid and included in the hash table.
     * Any polygons beyond that are discarded.
     */
    depth_bucket_node nodes[1024];
};
```

There are 2 separate hash tables: one for code that runs on the master CPU, and another for code that runs on the slave. Regardless of which hash table is used, the actual VDP1 command tables are stored in a common buffer that gets DMAed to VRAM.

Hash table for the master CPU: 0x060DD000
Hash table for the slave CPU: 0x060DF000

Likewise, there are 2 separate master copies of the code - one for master, another for the slave - that inserts commant tables into the sorted list. Since the command tables are in a common buffer, that means that the two routines are manipulating the same buffer concurrently. This could cause data corruption.