Master CPU sets up a buffer in the following format and sends a pointer
to it to the slave CPU:

```c++
struct slave_task_buffer {
	void (*task_fn)(void *);
	uint32_t params[17];
};
```

The Slave CPU copies the params to a dedicated work area then calls the
task_fn, passing it (R3) a pointer to the work area.

