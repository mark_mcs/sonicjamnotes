If left at the titlescreen with no user input the menu system will start playing demo sequences for each of the loadable modules. For Sonic World, this is achieved by setting the r4 register to 1 (boolean true) before calling the overlay entry point.

Demo sequence data is an array of 22-byte structures stored in DEMODATA.MUS, and gets loaded into low work RAM at 0x208000.

A counter is kept at offset 0x328 in the system variable area that determines which step in the demo sequence is being played back. There can be a maximum of 0x1746 (5,958) steps in the sequence. The routine at 0x060402d4 indexes into the array and loads each part of the structure into RAM.

```c++
struct demo_step {
    uint16_t ???; // copied to sva->0x1d8
    uint16_t ???; // copied to sva->GBR_SW_EXIT
    uint16_t ???; // copied to sva->0e4
    // remaining 16 bytes copied to 0x060ff930
    uint32_t input_flags[4] ???
};
```