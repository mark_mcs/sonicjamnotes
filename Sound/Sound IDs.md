## Sound Effects
| ID   | Description    |
| ---- | -------------- |
|      |                |
| 0x10 | Door open      |
|      |                |
| 0x1a | Ring           |
| 0x1b | Ring Alternate |
|      |                |
| 0x22 | Tails Flying   |