Contains sprite position data for compound objects?


| Offset | Type | Description|
|--------|------|------------|
| 0x00 | uint | Number of objects |
| 0x04 | uint | ??? |
| 0x08 | uint[] | Array of offsets to each entry. |
 | ... | object_def[] | data indexed by offsets array. |


| object_def |
|------|----|----|
| 0x00 | uint | number of sprites in the object |
| **sprite_entry** |
| 0x04 | uint | low word = sprite id |
| 0x04 | ushort | x offset |
| 0x08 | ushort | y offset |
