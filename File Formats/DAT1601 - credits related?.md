looks like gzip

| Offset | Type | Description |
|----|----|----|
| 0x00 | byte | Header length |
| 0x01 | byte | Checksum - sum of header bytes mod 256 |
| | |
| 0x05 | byte | Marker? Always 0x35 |
| | |
| 0x0B | int (le) | number of bits |
